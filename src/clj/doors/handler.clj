(ns doors.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [compojure.route :as route]
            [doors.env :refer [defaults]]
            [doors.layout :refer [error-page]]
            [doors.middleware :as middleware]
            [doors.routes.home :refer [home-routes]]
            [doors.routes.user :refer [user-routes]]
            [mount.core :as mount]))

(mount/defstate init-app
                :start ((or (:init defaults) identity))
                :stop  ((or (:stop defaults) identity)))

(def app-routes
  (routes
    (-> #'home-routes
        (wrap-routes middleware/wrap-csrf)
        (wrap-routes middleware/wrap-formats))
    (-> #'user-routes
        (wrap-routes middleware/wrap-formats))
    (route/not-found
      (:body
        (error-page {:status 404
                     :title "page not found"})))))


(defn app [] (middleware/wrap-base #'app-routes))
