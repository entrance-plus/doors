(ns doors.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[doors started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[doors has shut down successfully]=-"))
   :middleware identity})
