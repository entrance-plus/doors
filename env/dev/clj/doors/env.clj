(ns doors.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [doors.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[doors started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[doors has shut down successfully]=-"))
   :middleware wrap-dev})
