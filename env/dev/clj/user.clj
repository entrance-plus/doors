(ns user
  (:require [mount.core :as mount]
            [doors.figwheel :refer [start-fw stop-fw cljs]]
            doors.core))

(defn start []
  (mount/start-without #'doors.core/repl-server))

(defn stop []
  (mount/stop-except #'doors.core/repl-server))

(defn restart []
  (stop)
  (start))


