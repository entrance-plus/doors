(ns ^:figwheel-no-load doors.app
  (:require [doors.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
