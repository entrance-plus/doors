FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/doors.jar /doors/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/doors/app.jar"]
